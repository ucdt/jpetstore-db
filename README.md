# jpetstore-db

This repository holds the source code for jpetstore-db 1.0 and 1.1. jpetstore-db
contains the the database creation and load files for the JPetStore database and
is configired to use the IBM UCD DNUpgrader plugin. The versions are managed by
branches release_1.0 and release_1.1.